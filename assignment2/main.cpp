#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cassert>

#include "sort.hpp"
#include "utils.hpp"

using namespace std;

/* UTILITY FUNCTIONS */
/* Function to print an array */
void printArray(int A[], int size)
{
    int i;
    for (i=0; i < size; i++)
		cout << A[i] << endl;
}

/* Driver program to test above functions */
int main(int argc, char *argv[])
{
	bool first = true;
	int n, select, index=0;
	string line;
	int *v;
	Timer tmr;
	double t;

	if( argc < 2)
		select = 1; // default Quicksort
	else
		select = atoi(argv[1]);

		assert( select >= 1 && select <= 6);

	while(getline(cin, line))
	{
		istringstream iss( line );
		int value;
		while( iss >> value)
		{
			if(first)
			{
				n = value;
				v = new int[n];
				first = false;
			} else {
				v[index] = value;
				index++;
			}
		}
	}

	switch(select)
	{
		case 1:
			tmr.reset();
			QuickSort(v, n);
			t = tmr.elapsed();
			break;
		case 2:
			tmr.reset();
			HeapSort(v, n);
			t = tmr.elapsed();
			break;
		case 3:
			tmr.reset();
			MergeSort(v, n);
			t = tmr.elapsed();
			break;
		case 4:
			tmr.reset();
			InsertionSort(v, n);
			t = tmr.elapsed();
			break;
		case 5:
			tmr.reset();
			SelectionSort(v, n);
			t = tmr.elapsed();
			break;
        case 6:
            std::vector<int> arr (v, v+n);
            tmr.reset();
            std::sort(arr.begin(), arr.end());
            t = tmr.elapsed();
            break;

	}
	std::cout << std::fixed << std::setprecision(10) << t << std::endl;


	delete [] v;
	return 0;
}
