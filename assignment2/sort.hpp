#ifndef __SORT_H_INCLUDED__
#define __SORT_H_INCLUDED__

#include <vector>
#include <algorithm>

/* All implementations are authored by http://quiz.geeksforgeeks.org/

 * http://quiz.geeksforgeeks.org/merge-sort/
 * http://quiz.geeksforgeeks.org/selection-sort/
 * http://quiz.geeksforgeeks.org/insertion-sort/
 * http://quiz.geeksforgeeks.org/heap-sort/
 * http://quiz.geeksforgeeks.org/quick-sort/
 *
*/

void InsertionSort(int arr[], int n);
void QuickSort(int arr[], int n);
void HeapSort(int *a, int n);
void MergeSort(int arr[], int n);
void SelectionSort(int arr[], int n);


#endif
