#!/usr/bin/env bash
Sort=('Quick' 'Heap' 'Merge' 'Insertion' 'Selection' 'Default');

start=$(date +%s.%N)

for f in *.in; do
	for i in 1 2 3 4 5 6; do
		base_name=${f%.in}
		output="$(./main $n < $f)"
		echo  -e "Sort: ${Sort[$i-1]}\t \tTime: ${output} \t Input: ${base_name}"  
	done
done

dur=$(echo "$(date +%s.%N) - $start" | bc)
echo -e "Execution time: $dur seconds"