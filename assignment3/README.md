## Implementações dos algoritmos de ordenação

1. Counting Sort
2. Bucket Sort
3. Radix Sort

## Compilação:
```
$  make 
```

## Execução:
O programa recebe como argumento um inteiro **n** representado o algoritmo, com base na numeração acima. 
```
$  ./main n < input > ordered.out && diff ordered.out output
$  ./main 1 < data/example.in > ordered.out && diff ordered.out data/example.out
```

