#ifndef __SORT_H_INCLUDED__
#define __SORT_H_INCLUDED__

#include <vector>
#include <algorithm>

void CountingSort(int v[], int n);
void BucketSort(int v[], int n);
void RadixSort(int v[], int n);

#endif
