#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cassert>

#include "sort.hpp"

using namespace std;


void printArray(int A[], int size)
{
    for (int i=0; i < size; i++)
		cout << A[i] << endl;
}

/* Driver program to test above functions */
int main(int argc, char *argv[])
{
	bool first = true;
	int n, select, index=0;
	string line;
	int *v;

	if( argc < 2)
		select = 1; // Default CountingSort
	else
		select = atoi(argv[1]);

	assert( select >= 1 && select <= 3);

	while(getline(cin, line))
	{
		istringstream iss( line );
		int value;
		while( iss >> value)
		{
			if(first)
			{
				n = value;
				v = new int[n];
				first = false;
			} else {
				v[index] = value;
				index++;
			}
		}
	}
	switch(select)
	{
		case 1:
			CountingSort(v, n);
			break;
		case 2:
			BucketSort(v, n);
			break;
		case 3:
			RadixSort(v, n);
			break;
	}

	printArray(v,n);

	delete [] v;
	return 0;
}
