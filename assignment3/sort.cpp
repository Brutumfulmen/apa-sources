#include <algorithm>
#include <iostream>
#include "sort.hpp"


void CountingSort(int v[], int n)
{
    int i, j, k;
    int idx = 0;
    int min, max;

    min = max = v[0];
    for(i = 1; i < n; i++) {
        min = (v[i] < min) ? v[i] : min;
        max = (v[i] > max) ? v[i] : max;
    }

    k = max - min + 1;
    int *bucket = new int [k];
    for(i = 0; i < k; i++) bucket[i] = 0;

    for(i = 0; i < n; i++) bucket[v[i] - min]++;
    for(i = min; i <= max; i++)
        for(j = 0; j < bucket[i - min]; j++) v[idx++] = i;

    delete [] bucket;
}

void BucketSort(int v[], int n)
{
    int minValue = v[0];
    int maxValue = v[0];

    for (int i = 1; i < n; i++) {
        if (v[i] > maxValue)
            maxValue = v[i];
        if (v[i] < minValue)
            minValue = v[i];
    }

    int bucketLength = maxValue - minValue + 1;
    std::vector<int>* bucket = new std::vector<int>[bucketLength];

    for (int i = 0; i < bucketLength; i++) {
        bucket[i] = std::vector<int>();
    }

    for (int i = 0; i < n; i++) {
        bucket[v[i] - minValue].push_back(v[i]);
    }

    int k = 0;
    for (int i = 0; i < bucketLength; i++) {
        int bucketSize = bucket[i].size();
        if (bucketSize > 0) {
            for (int j = 0; j < bucketSize; j++) {
                v[k] = bucket[i][j];
                k++;
            }
        }
    }
}




/* Not Working
void RadixSort(int v[], int n)
{
    int *temp = new int[n];
    int largest = v[0];
    int exp = 1;

    for(int i=0; i<n; i++) {
        if(v[i] > largest) {
            largest = v[i];
        }
    }

    while( largest/exp > 0) {
        int bucket[10] = { 0 };
        for(int i=0; i<n; i++) {
            bucket[(v[i] / exp) % 10]++;
        }
        for(int i=0; i<10; i++) {
            bucket[i] += bucket[i-1];
        }
        for(int i=n-1; i>=0; i--) {
            temp[--bucket[(v[i] / exp) % 10]] = v[i];
        }
        for(int i=0; i<n; i++) {
            v[i] = temp[i];
        }
        exp *= 10;
    }
    delete [] temp;
}*/


int getMax(int v[], int n)
{
    int mx = v[0];
    for (int i = 1; i < n; i++)
        if (v[i] > mx)
            mx = v[i];
    return mx;
}


void countSort(int v[], int n, int exp)
{
    int output[n];
    int i, count[10] = {0};

    for (i = 0; i < n; i++)
        count[ (v[i]/exp)%10 ]++;

    for (i = 1; i < 10; i++)
        count[i] += count[i - 1];

    for (i = n - 1; i >= 0; i--)
    {
        output[count[ (v[i]/exp)%10 ] - 1] = v[i];
        count[ (v[i]/exp)%10 ]--;
    }

    for (i = 0; i < n; i++)
        v[i] = output[i];
}

void RadixSort(int v[], int n)
{
    int m = getMax(v, n);

    for (int exp = 1; m/exp > 0; exp *= 10)
        countSort(v, n, exp);
}