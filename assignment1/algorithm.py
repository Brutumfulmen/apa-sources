#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
import time


class Utils(object):

    @staticmethod
    def execution_time(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            start = time.time()
            return_value = func(*args, **kwargs)
            message = "Executing {} took {:.10f} seconds.\n".format(func.__name__,
                                                                 time.time() - start)
            print(message)
            return return_value
        return wrapper

    @staticmethod
    def swap_by_index(x, a, b):
        assert( a >= 0 and b > a)
        t = x[a]
        x[a] = x[b]
        x[b] = t


    @staticmethod
    def swap(a, b):
        assert(type(a) == type(b))
        c = a
        a = b
        b = c
        return a, b


class Sort(object):

    @staticmethod
    def quick_sort(list):
        """ Based on the pseudocode of page 171 of the Introduction to Algorithms book. """

        def partition(list, lo, hi):
            if not list:
                return None
            pivot = list[lo]
            left = lo+1
            right = hi
            done = False

            while not done:
                while left <= right and list[left] <= pivot:
                    left = left + 1
                while list[right] >= pivot and right >= left:
                    right = right - 1
                if right < left:
                    done = True
                else:
                    list[left], list[right] = Utils.swap(list[left], list[right])
            list[lo], list[right] = Utils.swap(list[lo], list[right])
            return right

        def recursive(list, lo, hi):
            if lo < hi:
                pivot = partition(list, lo, hi)
                recursive(list, lo, pivot-1)
                recursive(list, pivot+1, hi)
            return list

        if not list:
            return None
        n = len(list)
        return recursive(list, 0, n-1)


    @staticmethod
    def insertion_sort(list):
        """ Based on the pseudocode of page 18 of the Introduction to Algorithms book. """

        if not list:
            return None
        n = len(list)
        for i in range(0,n):
            cur = list[i]
            j = i
            while j > 0 and list[j-1] > cur:
                list[j] = list[j-1]
                j = j-1
            list[j] = cur
        return list

    @staticmethod
    def heap_sort(list):
        """ Based on the pseudocode of page 154 and 160 of the Introduction to Algorithms book. """
        def shift_down(list, lo, hi):
            largest = 2*lo+1
            while largest <= hi:
                if largest < hi and list[largest] < list[largest+1]:
                    largest = largest + 1
                if list[largest] > list[lo]:
                     list[largest], list[lo] = Utils.swap(list[largest], list[lo])
                     lo = largest
                     largest = 2*lo+1
                else:
                    return
        def heapsort(list):
            n = len( list ) - 1
            leastParent = int(n / 2)
            for i in range ( leastParent, -1, -1 ):
                shift_down(list, i, n )

            for i in range ( n, 0, -1 ):
                if list[0] > list[i]:
                    list[0], list[i] = Utils.swap(list[0], list[i])
                    shift_down(list, 0, i - 1)
            return list
        if not list:
            return None
        return heapsort(list)

    @staticmethod
    def merge_sort(list):
        """ Based on the pseudocode of page 31 and 34 of the Introduction to Algorithms book. """
        def recursive(list):
            if len(list)>1:
                mid = len(list)//2
                lefthalf = list[:mid]
                righthalf = list[mid:]

                recursive(lefthalf)
                recursive(righthalf)

                i, j, k = 0, 0, 0

                while i < len(lefthalf) and j < len(righthalf):
                    if lefthalf[i] < righthalf[j]:
                        list[k]=lefthalf[i]
                        i=i+1
                    else:
                        list[k]=righthalf[j]
                        j=j+1
                    k=k+1

                while i < len(lefthalf):
                    list[k]=lefthalf[i]
                    i=i+1
                    k=k+1

                while j < len(righthalf):
                    list[k]=righthalf[j]
                    j=j+1
                    k=k+1
            return list

        if not list:
            return None
        return recursive(list)

    @staticmethod
    def selection_sort(list):
        if not list:
            return None
        n = len(list)
        for i in range(0, n-1):
            min_ = i
            for j in range(i+1, n):
                if(list[j] < list[min_]):
                    min_ = j
            if min_ != i:
                list[i], list[min_] = Utils.swap(list[i], list[min_])
        return list
