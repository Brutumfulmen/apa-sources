#!/usr/bin/env python
# -*- coding: utf-8 -*-

from algorithm import Sort, Utils
import random


if __name__ == '__main__':

    # Initialize the random generator
    random.seed(None)

    # Length of List
    n = 10**4

    # Larger integer in list
    max_value = random.randint(0, n*2)

    # List with n random elements
    data = [ random.randint(0 ,max_value) for _ in range(0, n)]

    print("List with {} elements and with integers in the range [0,{}]\n".format(n, max_value))

    # Link to the decorator used in the measurement of the runtime function.
    qsort_timed = Utils.execution_time(Sort.quick_sort)
    hsort_timed = Utils.execution_time(Sort.heap_sort)
    isort_timed = Utils.execution_time(Sort.insertion_sort)
    ssort_timed = Utils.execution_time(Sort.selection_sort)
    msort_timed = Utils.execution_time(Sort.merge_sort)

    print(data)

    # Executes the algorithms.
    print(qsort_timed(data))
    print(hsort_timed(data))
    print(isort_timed(data))
    print(ssort_timed(data))
    print(msort_timed(data))

