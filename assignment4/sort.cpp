#include <algorithm>
#include "sort.hpp"


/*------------------------------------------------------------
 * Sort Algorithms with Comparison
 *
 *------------------------------------------------------------*/

void InsertionSort(int arr[], int n)
{
   int i, key, j;
   for (i = 1; i < n; i++)
   {
       key = arr[i];
       j = i-1;

       while (j >= 0 && arr[j] > key)
       {
           arr[j+1] = arr[j];
           j = j-1;
       }
       arr[j+1] = key;
   }
}

int partition (int arr[], int low, int high)
{
    int pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (arr[j] <= pivot)
        {
            i++;    // increment index of smaller element
            std::swap(arr[i], arr[j]);
        }
    }
    std::swap(arr[i + 1], arr[high]);
    return (i + 1);
}

void quickSort(int arr[], int low, int high)
{
    if (low < high)
    {
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

void QuickSort(int arr[], int n)
{
	return quickSort(arr, 0, n-1);
}

void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;

    /* create temp arrays */
    int L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];

    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int l, int r)
{
    if (l < r)
    {
        int m = l+(r-l)/2;

        // Sort first and second halves
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);

        merge(arr, l, m, r);
    }
}

void MergeSort(int arr[], int n)
{
	mergeSort(arr, 0, n-1);
}

void swap_xor(int &x, int &y)
{
    x ^= y;
    y ^= x;
    x ^= y;
}

void SelectionSort(int arr[], int n)
{
    int i, j, min_idx, cur;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n-1; i++)
    {
        // Find the minimum element in unsorted array
        min_idx = i;
        cur = arr[min_idx];
        for (j = i+1; j < n; j++)
          if (arr[j] < cur)
            min_idx = j;

        // Swap the found minimum element with the first element
        swap_xor(arr[min_idx], arr[i]);
    }
}

void heapify(int arr[], int n, int i)
{
    int largest = i;  // Initialize largest as root
    int l = 2*i + 1;  // left = 2*i + 1
    int r = 2*i + 2;  // right = 2*i + 2

    // If left child is larger than root
    if (l < n && arr[l] > arr[largest])
        largest = l;

    // If right child is larger than largest so far
    if (r < n && arr[r] > arr[largest])
        largest = r;

    // If largest is not root
    if (largest != i)
    {
        std::swap(arr[i], arr[largest]);

        // Recursively heapify the affected sub-tree
        heapify(arr, n, largest);
    }
}

// main function to do heap sort
void HeapSort(int arr[], int n)
{
    // Build heap (rearrange array)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // One by one extract an element from heap
    for (int i=n-1; i>=0; i--)
    {
        // Move current root to end
        std::swap(arr[0], arr[i]);

        // call max heapify on the reduced heap
        heapify(arr, i, 0);
    }
}

/*------------------------------------------------------------
 * Linear ordering algorithms
 *
 *------------------------------------------------------------*/

void CountingSort(int v[], int n)
{
    int i, j, k;
    int idx = 0;
    int min, max;

    min = max = v[0];
    for(i = 1; i < n; i++) {
        min = (v[i] < min) ? v[i] : min;
        max = (v[i] > max) ? v[i] : max;
    }

    k = max - min + 1;
    int *bucket = new int [k];
    for(i = 0; i < k; i++) bucket[i] = 0;

    for(i = 0; i < n; i++) bucket[v[i] - min]++;
    for(i = min; i <= max; i++)
        for(j = 0; j < bucket[i - min]; j++) v[idx++] = i;

    delete [] bucket;
}

void BucketSort(int v[], int n)
{
    int minValue = v[0];
    int maxValue = v[0];

    for (int i = 1; i < n; i++) {
        if (v[i] > maxValue)
            maxValue = v[i];
        if (v[i] < minValue)
            minValue = v[i];
    }

    int bucketLength = maxValue - minValue + 1;
    std::vector<int>* bucket = new std::vector<int>[bucketLength];

    for (int i = 0; i < bucketLength; i++) {
        bucket[i] = std::vector<int>();
    }

    for (int i = 0; i < n; i++) {
        bucket[v[i] - minValue].push_back(v[i]);
    }

    int k = 0;
    for (int i = 0; i < bucketLength; i++) {
        int bucketSize = bucket[i].size();
        if (bucketSize > 0) {
            for (int j = 0; j < bucketSize; j++) {
                v[k] = bucket[i][j];
                k++;
            }
        }
    }
}



int getMax(int v[], int n)
{
    int mx = v[0];
    for (int i = 1; i < n; i++)
        if (v[i] > mx)
            mx = v[i];
    return mx;
}


void countSort(int v[], int n, int exp)
{
    int output[n];
    int i, count[10] = {0};

    for (i = 0; i < n; i++)
        count[ (v[i]/exp)%10 ]++;

    for (i = 1; i < 10; i++)
        count[i] += count[i - 1];

    for (i = n - 1; i >= 0; i--)
    {
        output[count[ (v[i]/exp)%10 ] - 1] = v[i];
        count[ (v[i]/exp)%10 ]--;
    }

    for (i = 0; i < n; i++)
        v[i] = output[i];
}

void RadixSort(int v[], int n)
{
    int m = getMax(v, n);

    for (int exp = 1; m/exp > 0; exp *= 10)
        countSort(v, n, exp);
}
