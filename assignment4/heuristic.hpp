#ifndef __HEURISTIC_H_INCLUDED__
#define __HEURISTIC_H_INCLUDED__

#include <map>

std::map<int, int> GetCountElements(int arr[], int n);
std::map<int, double> GetProbabilities(int arr[], int n);

double GetMean (int arr[], int n);
double GetVariance (int arr[], int n);
double GetPercentageOrdered (int arr[], int n);
double GetEntropy (int arr[], int n);
void Reverse (int arr[], int n);
int	GetMinimum (int arr[], int n);
int GetMaximum (int arr[], int n);
int selectAlgorithm (int arr[], int n);

bool UniformlyDestributed( int arr[], int n);
bool MostRepeated(int arr[], int n);
bool SmallInput(int arr[], int n);
bool LargeInput(int arr[], int n);
bool HasNotNegative(int arr[], int n);

#endif // __HEURISTIC_H_INCLUDED__
