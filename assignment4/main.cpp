#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cassert>

#include "sort.hpp"
#include "heuristic.hpp"
#include "utils.hpp"

using namespace std;


void printArray(int A[], int size)
{
    for (int i=0; i < size; i++)
		cout << A[i] << endl;
}

/* Driver program to test above functions */
int main(int argc, char *argv[])
{
	bool first = true;
	int n, select, index=0;
	string line;
	int *v;
	Timer tmr;
	double t;
	bool heuristic = false;

	if( argc < 2) {
		heuristic = true;
	} else {
		select = atoi(argv[1]);
	}

	if(!heuristic) {
		assert( select >= 1 && select <= 8);
	}

	while(getline(cin, line))
	{
		istringstream iss( line );
		int value;
		while( iss >> value)
		{
			if(first)
			{
				n = value;
				v = new int[n];
				first = false;
			} else {
				v[index] = value;
				index++;
			}
		}
	}

	if(heuristic) {
		select = selectAlgorithm(v, n);
		switch(select)
		{
			case 1:
				tmr.reset();
				BucketSort(v, n);
				t = tmr.elapsed();
				break;
			case 2:
				tmr.reset();
				CountingSort(v, n);
				t = tmr.elapsed();
				break;
			case 3:
				tmr.reset();
				InsertionSort(v, n);
				t = tmr.elapsed();
				break;
			case 4:
				tmr.reset();
				QuickSort(v, n);
				t = tmr.elapsed();
				break;
			default:
				tmr.reset();
				QuickSort(v, n);
				t = tmr.elapsed();
				break;
		}
	} else {
		switch(select)
		{
			case 1:
				tmr.reset();
				BucketSort(v, n);
				t = tmr.elapsed();
				break;
			case 2:
				tmr.reset();
				CountingSort(v, n);
				t = tmr.elapsed();
				break;
			case 3:
				tmr.reset();
				InsertionSort(v, n);
				t = tmr.elapsed();
				break;
			case 4:
				tmr.reset();
				QuickSort(v, n);
				t = tmr.elapsed();
				break;
			default:
				tmr.reset();
				QuickSort(v, n);
				t = tmr.elapsed();
				break;
		}
	}

	// printArray(v,n);
	std::cout << select << "\n";
	std::cout << std::fixed << std::setprecision(10) << t << std::endl;

	delete [] v;
	return 0;
}
