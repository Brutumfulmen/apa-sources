#ifndef __SORT_H_INCLUDED__
#define __SORT_H_INCLUDED__

#include <vector>
#include <algorithm>

// Sort Algorithms with Comparison
void InsertionSort(int arr[], int n);
void QuickSort(int arr[], int n);
void HeapSort(int arr[], int n);
void MergeSort(int arr[], int n);
void SelectionSort(int arr[], int n);

// Linear ordering algorithms
void CountingSort(int v[], int n);
void BucketSort(int v[], int n);
void RadixSort(int v[], int n);

#endif
