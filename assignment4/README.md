## Implementação de uma Heurística seletora de algoritmo de ordenação

Elaborar um heurística seletora de algoritmo de ordenação com as seguintes propriedades:

1. Priorizar ordenação linear, se possível;
2. Usar rotinas auxiliares com complexidade O(n);
3. Se necessário, pré-processar os dados de entrada;

Regras de Implementação:

1. Nenhum parâmetro será passado como argumento;
2. A entrada e saída utilizada serão as padrões;
3. Linguagem deverá ser uma das usadas anteriormente; 

## Proposta
1. Se os elementos distribuídos na faixa [0,1), então use Bucket Sort;
2. Se existe grande quantidade de valores repetidos, então use Count Sort;
3. Se a quantidade de elementos é pequena, então Insertion Sort;
	1. Eficiente para uma entrada pequena e na maioria ordenada;
4. Se a quantidade de elementos é grande, então Heap Sort, Quick Sort ou Merge Sort
	1. Então, escolha Quick Sort, pois é o algoritmo mais rápido, na prática

## Compilação:
```
$  make 
```

## Execução:

1. Bucket Sort
2. Counting Sort
3. Insertion Sort
4. Selection Sort

O programa recebe como argumento um inteiro **n** representado o algoritmo, 
com base na numeração acima. Se não for passado o parâmetro inteiro **n** o programa escolherá o algoritmo de ordenação com base na
heurística proposta acima.

```
$  ./main < input > ordered.out && diff ordered.out output
$  ./main n < input > ordered.out && diff ordered.out output
$  ./main 1 < data/example.in > ordered.out && diff ordered.out data/example.out
```
