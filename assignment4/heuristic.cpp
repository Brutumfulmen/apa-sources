
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>

#include "heuristic.hpp"

#include <iostream>


struct Greater {
	bool operator()(const long& a, const long& b) const {
		return a > b;
	}
};

double GetMean(int arr[], int n)
{
	double sum = 0.0;

	for(int i=0; i<n; i++)
		sum += arr[i];

	return sum/n;
}

double GetVariance(int arr[], int n)
{
	double mean = GetMean(arr, n);
	double sum = 0.0;

	for(int i=0; i<n; i++) {
		sum += (arr[i]-mean)*(arr[i]-mean);
	}
	return sum /(n-1);
}

double GetPercentageOrdered(int arr[], int n)
{
    long correct = 0;

    // Make vector from array
    std::vector<int> v(arr, arr+sizeof(arr)/sizeof(arr[0]));
    // Make heap from vector
    make_heap(v.begin(), v.end(), Greater());

    for(int i=0; i<n; i++)
    {
        if(v.size()) {
            int min = v.back();
            if( arr[i] == min) {
                correct += 1;
            }
            std::pop_heap(v.begin(), v.end(), Greater());
            v.pop_back();
        }
    }
    return (double)correct / n;
}

double GetEntropy(int arr[], int n)
{
    double sum = 0.0;
    std::map<int, double> probabilities = GetProbabilities(arr, n);

    for(int i=0; i<n; i++) {
        auto it = probabilities.find(arr[i]);
        if(it != probabilities.end()) {
            if( it->second != 0) {
                sum += std::log2(it->second);
            }
        }
    }
    double entropy = (sum==0) ? 0 : (-1.0)*sum;
    return entropy;
}

std::map<int, double> GetProbabilities(int arr[], int n)
{
    std::map<int, int> count = GetCountElements(arr, n);
    std::map<int, double> prob;

    for(int i=0; i<n; i++) {
        auto it = count.find(arr[i]);
        if(it != count.end()) {
            prob.insert(std::pair<int, double>(arr[i], (double)it->second/n));
        } else {

        }
    }
    return prob;
}

void Reverse(int arr[], int n)
{
	for(int i=0; i<n/2; i++) {
		int tmp = arr[i];
		arr[i] = arr[n-1-i];
		arr[n-i-1] = tmp;
	}
}

std::map<int, int> GetCountElements(int arr[], int n)
{
    std::map<int, int> count;
    for(int i=0; i<n; i++) {
        auto it = count.find(arr[i]);
        if(it != count.end()) {
            it->second += 1;
        } else {
            count.insert(std::pair<int, int>(arr[i], 1));
        }
    }
    return count;
}

int GetMinimum(int arr[], int n)
{
	int min = arr[0];
	for(int i=1; i<n; i++) {
		if(min > arr[i]) {
			min = arr[i];
		}
	}
	return min;
}

int GetMaximum(int arr[], int n)
{
	int max = arr[0];
	for(int i=1; i<n; i++) {
		if(max < arr[i]) {
			max = arr[i];
		}
	}
	return max;
}

bool UniformlyDestributed( int arr[], int n)
{
    double threshold = 0.8;
    int min = GetMinimum(arr, n);
    int max = GetMaximum(arr, n);
    double var = GetVariance(arr, n);
    int len = (max - min) + 1;
    double rate = abs(len-var);
    //std::cout << "Rate " << rate << std::endl;
    if( rate <= threshold) {
        return true;
    }
    return false;

}
bool MostRepeated(int arr[], int n)
{
    int threshold_repeated = 2;
    double threshold_percentage = 0.8;

    int count_largest_threshold = 0;

    std::map<int, int> count = GetCountElements(arr, n);
    for(auto it = count.begin(); it!=count.end(); it++)
    {
        if(it->second > threshold_repeated) {
             count_largest_threshold += 1;
        }
    }
    if( count_largest_threshold/n >= threshold_percentage) {
        return true;
    }
    return false;

}
bool SmallInput(int arr[], int n)
{
    int threshold = 50;
    if( n <= threshold) {
        return true;
    }
    return false;
}
bool LargeInput(int arr[], int n)
{
    int threshold = 51;
    if(n >= threshold) {
        return true;
    }
    return false;

}
bool HasNotNegative(int arr[], int n)
{
    int min = GetMinimum(arr, n);
    if( min < 0) {
        return false;
    }
    return true;
}

int selectAlgorithm(int arr[], int n)
{
    bool isSmall = SmallInput(arr, n);
    bool isLarge = LargeInput(arr, n);
    bool isUnfds = UniformlyDestributed(arr, n);
    bool isReptd = MostRepeated(arr, n);

    if( isUnfds ) {
        return 1;
    }
    else if( isLarge and isReptd ) {
        return 2;
    }
    else if( isLarge and isUnfds) {
        return 1;
    }
    else if( isSmall ) {
        return 3;
    }
    else {
        return 4;
    }
}
